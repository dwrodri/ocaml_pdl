(* Derek Rodriguez 
 * SDE 2
 * Due April 11th, 2019
 *)

(* Print functions that I used for
 * convenient intermediate debugging.
 * They aren't used in my codebase
 * but I'm leaving them here anyways.
 *)
let rec print_list = function
  | [] ->
      ()
  | e :: l ->
      print_string e ;
      print_string " " ;
      print_list l
;;


let rec print_int_list = function
  | [] ->
      ()
  | e :: l ->
      print_int e ;
      print_string " " ;
      print_int_list l
;;

let print_consec_out (x, y) =
  print_list x ;
  print_string " " ;
  print_int y ;
  print_char '\n'
;;

(* Auxiliary functions *)

let map (alist, fn) =
  let rec aux (alist, accumulated) =
    match alist with
    | [] ->
        accumulated
    | hd :: tl ->
        aux (tl, accumulated @ [fn hd])
  in
  aux (alist, [])
;;


let rec any (alist, to_boolean) =
  match alist with
  | [] ->
      false
  | hd :: tl ->
      if to_boolean hd then true else any (tl, to_boolean)
;;

let rec all (alist, checker) =
  let rec aux (blist, checker) =
    match blist with
    | hd :: tl when checker hd ->
        aux (List.tl blist, checker)
    | [] when alist != [] ->
        true
    | _ ->
        false
  in
  aux (alist, checker)
;;


(* Here are the generator functions *)

let gen_string (n, what) =
  let rec extend (n, what, list_boi) =
    if n > 0 then extend (n - 1, what, list_boi @ [what]) else list_boi
  in
  extend (n, what, [])
;;


let gen_square n =
  gen_string (n, "u")
  @ gen_string (n, "r")
  @ gen_string (n, "d")
  @ gen_string (n, "l")
;;


let gen_rect (n, m) =
  gen_string (n, "u")
  @ gen_string (m, "r")
  @ gen_string (n, "d")
  @ gen_string (m, "l")
;;


(* Counting functions 3.4 , 3.5 *)

let rec countups (alist, n) =
  match alist with
  | [] ->
      n
  | "u" :: tail ->
      countups (tail, n + 1)
  | _ :: tail ->
      countups (tail, n)
;;


let rec consec_counts (alist, n, what) =
  match alist with
  | head :: tail when n = 0 ->
      if head = what
      then consec_counts (tail, n + 1, what)
      else consec_counts (tail, n, what)
  | [] ->
      ([], n)
  | [head] ->
      if head = what then ([], n + 1) else ([head], n)
  | head :: (next :: rest as tail) ->
      if next = what && head = what
      then consec_counts (rest, n + 2, what)
      else if head = what
      then consec_counts (tail, n + 1, what)
      else (head :: tail, n)
;;

(* I made these because we're not supposed to have curried functions *)
let get_consec_remainder (alist, n) = alist;;
let get_consec_count (alist, n) = n;;


(* This was the original generic grammar matching function.
 * It eventually got replace by exact_grammar_match. The idea 
 * is to transform a string list into a list of counts collected 
 * from consec_counts, then every shape-detecting function only 
 * needs to pass in the symbols as they appear in order, and do
 * some basic control flow with the letter counts. 
 *)
let rec grammar_match (alist, nlist, charlist) =
  if List.length charlist != 0
  then
    grammar_match
      ( get_consec_remainder (consec_counts (alist, 0, List.hd charlist))
      , get_consec_count (consec_counts (alist, 0, List.hd charlist)) :: nlist
      , List.tl charlist )
  else nlist
;;

(* The only difference between these two is that this one allows no leftovers *)
let rec exact_grammar_match (alist, nlist, charlist) =
  if List.length charlist != 0
  then
    exact_grammar_match
      ( get_consec_remainder (consec_counts (alist, 0, List.hd charlist))
      , get_consec_count (consec_counts (alist, 0, List.hd charlist)) :: nlist
      , List.tl charlist )
  else if alist = []
  then nlist
  else [-1]
;;

(* These are the basic shape detector functions.
 * Note that I use -1 as an error value in exact_grammar_match.
 *)

(* sq ended up using the old grammar_match because it allowed leftovers. *)
let sq alist =
  all (grammar_match (alist, [], ["u"; "r"; "d"; "l"]), fun x -> x != 0)
;;


let sq_all alist =
  all (exact_grammar_match (alist, [], ["u"; "r"; "d"; "l"]), fun x -> x > 0)
;;


let sqA alist =
  all
    ( exact_grammar_match (alist, [], ["u"; "r"; "d"; "l"])
    , fun a ->
        a = List.hd (exact_grammar_match (alist, [], ["u"; "r"; "d"; "l"]))
    && a > 0 )
;;


let eqtriA alist =
  all
    ( exact_grammar_match (alist, [], ["u"; "m30"; "p240"])
    , (fun a ->
        a
        = ( List.hd (exact_grammar_match (alist, [], ["u"; "m30"; "p240"]))
           ) && a > 0))
;;


(* Shifting functions *)
let one_shift alist = List.tl alist @ [List.hd alist];;

let all_shifts alist =
  let rec aux (blist, shifts, count) =
    if count > 0
    then aux (one_shift blist, [one_shift blist] @ shifts, count - 1)
    else shifts
  in
  aux (alist, [], List.length alist - 1)
;;


let all_cases alist =
  let rec aux (blist, shifts, count) =
    if count > 0
    then aux (one_shift blist, [one_shift blist] @ shifts, count - 1)
    else shifts
  in
  aux (alist, [], List.length alist)
;;

(* The Final functions, which are quite simple
 * given that they leverage all my prior code
 *)
let try_all_sqA alist = any(all_cases alist, sqA);;


let try_all_eqtriA alist = any(all_cases alist, eqtriA);;
