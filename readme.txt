***CONTENTS OF ARCHIVE***
- which_shape.caml: ocaml source file with function implementations
- dwrodri-sde2.log: Log file showing 2 sample uses of every mentioned function in the design spec, edited for brevity
- readme.txt: this document

Pledge:
On my honor, I have neither given nor received aid on this exam.
